[PT RIFAN FINANCINDO BERJANGKA PALEMBANG](http://www.rfinancindoberjangka.com/) merupakan salah satu cabang PT Rifan Financindo Berjangka anggota Bursa Berjangka Jakarta dan Lembaga Kliring Berjangka yang mengadakan transaksi kontrak berjangka secara teratur, wajar, efektif, transparan dan tercatat di Bursa Berjangka Jakarta, yang diatur dalam perundang-undangan dibidang Perdagangan Berjangka sehingga memberikan kepastian hukum kepada semua pihak yang melakukan kegiatan Perdagangan Berjangka Komoditi di Indonesia.

Dalam mendukung pemerintah untuk membangun kembali iklim investasi di Indonesia, maka sejak awal tahun 2000 Rifan Financindo Group melalui PT. Rifan Financindo Berjangka mulai bergiat mengkhususkan diri sebagai Perusahaan Pialang yang berorientasi pada jasa pelayanan bagi seluruh masyarakat yang ingin memanfaatkan peluang dalam bertransaksi di pasar komoditi maupun derivatif di tanah air.

Di dukung teknologi informasi dan sumber daya manusia yang profesional dan telah memenuhi standar kualifikasi kepatutan dan kecakapan dari Badan Pengawas Perdagangan Berjangka Komoditi (BAPPEBTI). PT. Rifan Financindo Berjangka merupakan aset bagi masyarakat pemodal yang ingin berinvestasi pada industri bursadi Indonesia maupun bursa mancanegara, serta senantiasa berusaha menempatkan diri sebagai perusahaan pialang yang terpercaya, berkualitas dan dapat diandalkan.

Follow Us :

[Facebook](https://www.facebook.com/RifanFinancindoPalembang)

[Twitter](https://twitter.com/Rifan_Palembang)

[Instagram](https://www.instagram.com/rifanfinancindoberjangkaplm/)